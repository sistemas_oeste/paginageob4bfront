(function() {
  'use strict';

  angular
    .module('webgeoB4BFront', [
      'ngAnimate', 
      'ngCookies', 
      'ngTouch', 
      'ngSanitize', 
      'ngMessages', 
      'ngAria',
      'ngResource', 
      'ui.router', 
      'toastr',
      'ui-leaflet']);

})();
