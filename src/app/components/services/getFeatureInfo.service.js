(function () {
    'use strict';

    angular.module('webgeoB4BFront')
        .factory('getFeatureInfoService', function ($http, $q)
    {
      return{

        GetFeatureInfo: function (url,service,version,request,info_format,query_layers,styles,viewparams,layers,x,y,srs,width,height,bbox) {
          var dfd = $q.defer();
          var config = {
            method : "GET",
            url : "https://cors-anywhere.herokuapp.com/https://api.4tour360.com/ewisemapsGetFeatureInfo/api/getFeatureInfoLayers?"
            +"url="+ url
            +"&service="+service
            +"&version="+version
            +"&request="+request
            +"&info_format="+info_format
            +"&query_layers="+query_layers
            +"&styles="+styles
            +"&viewparams="+viewparams
            +"&layers="+layers
            +"&x="+x
            +"&y="+y
            +"&SRS="+srs
            +"&WIDTH="+width
            +"&HEIGHT="+height
            +"&BBOX="+bbox+""
            +"&feature_count="+1+""
          };
          $http(config)
            .then(
              function (response) {
                return dfd.resolve(response);
                
              },
              function (errResponse) {
                return dfd.reject(errResponse);
              }
            );
          return dfd.promise;
        }
      }
          
              
    });


})();