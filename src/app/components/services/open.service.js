(function () {
    'use strict';

    angular.module('webgeoB4BFront')
        .factory('servicesOpen', function ($http, $q)
    {
      return{
        SendEmail: function (name,last_name, company_name, email,phone, message){
          var dfd = $q.defer();
          var config = {
            method : "POST",
            url : "https://cors-anywhere.herokuapp.com/https://api.4tour360.com/geob4b/api/contact?name="+name+"&last_name="+last_name+"&company_name="+company_name+"&e_mail="+email+"&phone="+phone+"&message="+message+"",
            headers: {
              'Content-Type': 'application/json; charset=utf-8'
            },
            jsonpCallbackParam: 'JSON_CALLBACK'
          };
          $http(config)
            .then(
              function (response) {
                return dfd.resolve(response);
                
              },
              function (errResponse) {
                return dfd.reject(errResponse);
              }
            );
          return dfd.promise;
        },
        UpdateBlog: function (){
          var dfd = $q.defer();
          var config = {
            method : "GET",
            url : "https://cors-anywhere.herokuapp.com/https://oeste.xyz/data/blog/all-min.json",
            headers: {
              'Content-Type': 'application/json; charset=utf-8'
            },
            jsonpCallbackParam: 'JSON_CALLBACK'
          };
          $http(config)
            .then(
              function (response) {
                return dfd.resolve(response);
                
              },
              function (errResponse) {
                return dfd.reject(errResponse);
              }
            );
          return dfd.promise;
        }
      }
          
              
    });


})();