(function() {
  'use strict';

  angular
    .module('webgeoB4BFront')
    .controller('MainController', MainController);


  function MainController($scope, $rootScope,servicesOpen, toastr,staticize,$location,$log,leafletMapEvents,leafletData) {
    
    var vm = $scope;
    var rs  = $rootScope;


    vm.bounds = {
      northEast : {
        lat:18.6670631919266 , 
        lng:-111.15966796875
      },
      southWest : {
        lat:28.323724553546015 ,
        lng:-92.26318359375
      }
    }


    vm.defaults = {
      zoomsliderControl: true,
      zoomControlPosition: 'bottomright',
      scrollWheelZoom: true,
      controls: {
        layers: {
            visible: false
        }
      },
      doubleClickZoom: false,
      map: {
        contextmenu: true,
        contextmenuWidth: 140,
        contextmenuItems: [{
            text: 'Analizar este lugar',
            callback: analyzeThisPlace
        }]
      }
    } 

    //Menu Context Function
    function analyzeThisPlace(e) {
      var center = {lat: e.latlng.lat, lng: e.latlng.lng};
      vm.analyzeThisPlace(center);
    }

    vm.layers = {
      baselayers: {},
      overlays: [
        {
          name: "Empresas",
          type: "wms",
          visible: true,
          url: "https://demo.ewisemaps.com/geoserver/ewisemaps/wms?",
          layerParams: {
            layers: "ewisemaps:b4b_denue",
            format: "image/png",
            transparent: true,
            viewparams:"",
            styles: "iconfinder",
            doRefresh: true
          },
          layerOptions: {
            maxZoom: 18
          },
          group: "Inmuebles"
        }
      ]
    }

    //Default
    vm.radius = 150;
    vm.customize = '#cccccc';

    //Draw
    vm.analysis_zone = function(center,radius, customize,map){
      angular.forEach(vm.layers.overlays, function(x){
        x.visible = true;
        x.doRefresh = true;
        x.layerParams.viewparams = "lo:"+center.lat+";la:"+center.lng+";m:"+radius;
      })

      if(vm.circle != undefined){
        map.removeLayer(vm.circle)
        vm.circle = new L.circle([center.lat,center.lng],radius,{color:customize, opacity:0.1});
        return vm.circle;
      }
      else{
        vm.circle = new L.circle([center.lat,center.lng],radius,{color:customize, opacity:0.1});
        return vm.circle;
      }
    }

    

    //getFeatureInfo
    vm.getFeautreInfo = function(x,y, z){
      vm.gfi = {e : [],l : [], b: []};
      vm.gfi = {e : x,l : y , b : z};
      rs.$emit('getFeatureInfo', vm.gfi);
    }


    //Add Data into Map
    leafletData.getMap().then(function(map) {

      var basemaps = [
        L.tileLayer('https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
          label: 'Google Maps'
        }),
        L.tileLayer('https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
          label: 'Google Satellite'
        })
      ];

      L.control.basemaps({
        basemaps: basemaps,
        tileX: 0,
        tileY: 0,
        tileZ: 1,
        position : 'bottomleft'
      }).addTo(map);

      //Geocoder Command
      L.Control.geocoder({
        collapsed : false,
        position : 'topleft',
        text: 'Locate',
        placeholder : 'Buscar una dirección',
        errorMessage : 'Ningún resultado :(',
        geocoder : new L.Control.Geocoder.Bing('AuDG4yiuhhkvFKrbESLaDJtOEl7k_dOCYYmCVUtbkO2oHLfXfos-6_vq0gh8hS1C')
      })
      .on('markgeocode', function(e) {
        vm.analysis_zone(e.geocode.center,vm.radius,vm.customize,map).addTo(map);
      })
      .addTo(map);

      //GPS
      L.control.locate({
        position: 'bottomright',
        icon : 'ion-location',
        drawCircle : false,
        drawMarker : false,
        getLocationBounds: function(t) {
          var center = {lat: t.latitude, lng: t.longitude};
          vm.analysis_zone(center,vm.radius,vm.customize,map).addTo(map);
          return t.bounds;
        }
      }).addTo(map).start();


      //Analyze This Place
      vm.analyzeThisPlace = function(center){
        vm.analysis_zone(center,vm.radius,vm.customize,map).addTo(map);
      }
      
      //Click Action
      map.on('singleclick',function ( e ) { 
        vm.getFeautreInfo(e, vm.layers.overlays,map.getBounds());

        rs.$on('featureInfo', function(x,data){
          if(data != '' && data != null & data != undefined){
            L.popup().setLatLng( e.latlng )
            .setContent( 
              '<div id="geoB4B-pop-up-feature-info" style="width:100%">'
                +data
              +'</div>' ) 
            .openOn( map );
          }
          
        })
        
      });

      //events
      vm.events = {
        map: {
            enable: ["click","dblclick","mousedown","mouseup","mouseover","mouseout",
            "mousemove","contextmenu","focus","blur","preclick","load","unload",
            "viewreset","movestart","move","moveend","dragstart","drag","dragend",
            "zoomstart","zoomanim","zoomend","zoomlevelschange","resize","autopanstart",
            "layeradd","layerremove","baselayerchange","overlayadd","overlayremove",
            "locationfound","locationerror","popupopen","popupclose","draw:created",
            "draw:edited","draw:deleted","draw:drawstart","draw:drawstop",
            "draw:editstart","draw:editstop","draw:deletestart","draw:deletestop"],
            logic: 'emit'
        }
      }
    
      for (var k in leafletMapEvents.getAvailableMapEvents()){
        var eventName = 'leafletDirectiveMap.' + leafletMapEvents.getAvailableMapEvents()[k];
        $scope.$on(eventName, function(event){
            rs.$emit('eventoMouse', event.name)
        });
      }

      

    });


    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;

    vm.url =$location.path();

    vm.quotation = {
      name : '',
      last_name : '',
      email : '',
      message : '',
      phone : '',
      company : ''
    }

    vm.sendQuotation = function(){
      if(vm.quotation.name != 'null'  && vm.quotation.email != '' && vm.quotation.message != ''){
        vm.promise = servicesOpen.SendEmail(vm.quotation.name, vm.quotation.last_name, 
          vm.quotation.company , vm.quotation.email, vm.quotation.phone, vm.quotation.message).then(function successCallback(response) {
          $log.log(response)
          vm.quotation = {
            name : '',
            last_name : '',
            email : '',
            message : '',
            phone : '',
            company : ''
          }
          if(response.data.message == 'success'){
            angular.element( document.querySelector('#geoB4B-verify')).css('display','flex');
            
          }
          else{
            toastr.error('¡Ocurrió un error, recarga la página!');
          }
          
        });
      }
    };

    vm.checking = function(){
      angular.element( document.querySelector('#geoB4B-verify')).css('display','none');
    }

    vm.promise = servicesOpen.UpdateBlog().then(function successCallback(response) {
      vm.blog = response.data[0];
    });

    staticize.setFinished();
    
  }
})();
