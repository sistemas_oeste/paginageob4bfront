(function() {
    'use strict';
  
    angular
      .module('webgeoB4BFront')
      .controller('GetPopUpFeatureInfoController', GetPopUpFeatureInfoController);

  
    function GetPopUpFeatureInfoController($scope, $rootScope,staticize,$filter) {
      
        var vm = $scope;
        var rs = $rootScope;


        rs.$on('infoLayers', function(e,d){
            vm.panel_info_layers = [];
            vm.panel_info_layers = d;
            vm.featureInfo = [];
            if(vm.panel_info_layers.length >= 1){
                angular.forEach(vm.panel_info_layers, function(x){
                    angular.forEach(x.items , function(y){
                        vm.featureInfo =
                            '<div class="pop-up-feature-info-item">'+
                                '<h4>'+y.properties.denue_raz_social+'</h4>'+
                                '<table>'+
                                    '<tr>'+
                                        '<td><b>Actividad</b></td>'+ '<td>'+y.properties.denue_nombre_act+'</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td><b>Sitio Web</b></td>'+ '<td>'+$filter('lowercase')(y.properties.denue_www)  +'</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td><b>e-mail</b></td>'+ '<td>'+$filter('lowercase')(y.properties.denue_correoelec)+'</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td><b>Teléfono</b></td>'+ '<td>'+y.properties.denue_telefono+'</td>'+
                                    '</tr>'+
                                '</table>'+
                            '</div>'
                        ;
                        rs.$emit('featureInfo', vm.featureInfo);
                    })
                })
            }
            else{
                rs.featureInfo = ''; 
                rs.$emit('featureInfo', vm.featureInfo);              
            }
        });


        staticize.setFinished();

    }
})();