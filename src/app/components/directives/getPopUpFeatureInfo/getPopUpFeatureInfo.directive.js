(function () {
    'use strict';
  
    angular.module('webgeoB4BFront')
        .directive('getPopUpFeatureInfoDirective', getPopUpFeatureInfoDirective);
  
    function getPopUpFeatureInfoDirective() {
      return {
        restrict: 'E',
        templateUrl: 'app/components/directives/getPopUpFeatureInfo/getPopUpFeatureInfo.html',
        controller:'GetPopUpFeatureInfoController'
      };
    }
  
})();