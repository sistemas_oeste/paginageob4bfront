(function () {
    'use strict';
  
    angular.module('webgeoB4BFront')
        .directive('getFeatureInfoDirective', getFeatureInfoDirective);
  
    function getFeatureInfoDirective() {
      return {
        restrict: 'E',
        templateUrl: 'app/components/directives/getFeatureInfo/getFeatureInfo.html',
        controller:'GetFeatureInfoController'
      };
    }
  
})();