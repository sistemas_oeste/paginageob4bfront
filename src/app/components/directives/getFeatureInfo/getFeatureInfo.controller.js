(function() {
    'use strict';
  
    angular
      .module('webgeoB4BFront')
      .controller('GetFeatureInfoController', GetFeatureInfoController);

  
    function GetFeatureInfoController($scope, $rootScope,toastr,staticize,getFeatureInfoService,$log) {
      
        var vm = $scope;
        var rs = $rootScope;

        rs.$on('getFeatureInfo', function(e,d){
            
            var bboxString = '';
            var viewLayers = [];
            var infoLayers = [];

            bboxString = d.b._southWest.lng+'%2C'+d.b._southWest.lat+'%2C'+d.b._northEast.lng+'%2C'+d.b._northEast.lat+'';

            angular.forEach(d.l, function(x){
                if(x.visible == true){
                    viewLayers.push(x)
                }
            });

            if(viewLayers.length >= 1){
                angular.forEach(viewLayers, function(x){
                    getFeatureInfoService.GetFeatureInfo(
                        x.url,
                        x.type, 
                        '1.1.1', 
                        'GetFeatureInfo',
                        'text/javascript', 
                        x.layerParams.layers, 
                        x.layerParams.styles, 
                        x.layerParams.viewparams, 
                        x.layerParams.layers,
                        Math.round(d.e.containerPoint.x), 
                        Math.round(d.e.containerPoint.y),
                        'EPSG:4326',
                        d.e.target._size.x, 
                        d.e.target._size.y, 
                        bboxString
                    ).then(function successCallback(response) {
                        if(response.data.features.length >= 1){
                            infoLayers.push({
                                name : x.name,
                                template :x.layerParams.layers,
                                items :response.data.features
                            });
                            angular.element( document.getElementsByClassName('angular-leaflet-map')).css('cursor','pointer');
                            rs.$emit('infoLayers', infoLayers);
                        }
                        else{
                            rs.$emit('infoLayers', infoLayers);
                        }
                    });
                });
            }
            else{
                rs.$emit('infoLayers', infoLayers);
                angular.element( document.getElementsByClassName('angular-leaflet-map')).css('cursor','pointer');
            }
        })

        staticize.setFinished();

    }
})();
  