(function() {
    'use strict';
  
    angular
      .module('webgeoB4BFront')
      .controller('GetMapEventsController', GetMapEventsController);

  
    function GetMapEventsController($scope, $rootScope, staticize) {
      
        var vm = $scope;
        var rs  = $rootScope;

        rs.$on('eventoMouse', function(e,d){
            switch(d){
                case 'leafletDirectiveMap.click':
                    angular.element( document.getElementsByClassName('angular-leaflet-map')).css('cursor','progress');
                break;
                case 'leafletDirectiveMap.drag':
                    angular.element( document.getElementsByClassName('angular-leaflet-map')).css('cursor','-webkit-grabbing');
                break;
                case 'leafletDirectiveMap.mousemove':
                    angular.element( document.getElementsByClassName('angular-leaflet-map')).css('cursor','pointer');
                break;
                default:
                    angular.element( document.getElementsByClassName('angular-leaflet-map')).css('cursor','pointer');
                break;
            }
         })

      staticize.setFinished();
      
    }
})();
  