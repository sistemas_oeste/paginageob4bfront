(function () {
    'use strict';
  
    angular.module('webgeoB4BFront')
        .directive('getMapEventsDirective', getMapEventsDirective);
  
    function getMapEventsDirective() {
      return {
        restrict: 'E',
        templateUrl: 'app/components/directives/getMapEvents/getMapEvents.html',
        controller:'GetMapEventsController'
      };
    }
  
})();