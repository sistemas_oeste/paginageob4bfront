'use strict';

 angular.module('webgeoB4BFront')
  /**
   * Staticize service.
   * Prepend to the body a div#finished to allow gulp/seo.js to create snapshots after this div appears.
   */
  .factory('staticize', function ($timeout, $state) {
    var staticize = {
      setFinished: function(){
        var thisState = $state.current.name;
        $timeout(function(){
          if($state.is(thisState)){
            if(angular.element('#finished').length === 0){
              angular.element('body').prepend('<div id="finished" style="position:absolute">&nbsp;</div>');
            }
          }
        }, 200);
      }
    }

    return staticize;
  });