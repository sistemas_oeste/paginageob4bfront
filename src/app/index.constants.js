(function() {
  'use strict';

  angular
    .module('webgeoB4BFront')
    .constant('moment', moment);

})();
