(function() {
  'use strict';

  angular
    .module('webgeoB4BFront')
    .config(routerConfig);


  function routerConfig($stateProvider, $urlRouterProvider,$locationProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/components/unsecure/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      });
    
    $urlRouterProvider.when('', '/');
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);

  }

})();
