// Add this file to gulp/seo.js

'use strict';

var conf = require('./conf');
var gulp = require('gulp');
var gutil = require('gulp-util');
var path = require('path');
var htmlSnapshots = require('html-snapshots');
var through = require('through2');
var PluginError = gutil.PluginError;
var runSequence = require('run-sequence');
var rename = require('gulp-rename');
var sitemap = require('gulp-sitemap');

// A custom gulp plugin to make snapshots
function gulpSnapshots(options) {
  // Creating a stream through which each file will pass
  return through.obj(function(file, enc, cb) {
    var self = this;

    // If file is null, go ahead with it
    if (file.isNull()) {
      self.push(file);
      cb(); // Done.
    }

    // If file is buffer, process!
    if (file.isBuffer()) {

      var pageList = JSON.parse(file.contents.toString()); //eslint-disable-line

      // Prepend the host URL to each page
      for (var i = 0; i < pageList.length; i++) {
        pageList[i] = options.siteUrl + pageList[i];
      }

      gutil.log(gutil.colors.blue('Creating ' + gutil.colors.bold(pageList.length) + ' snapshots.'));

      var outputDir = path.join(__dirname, '../' + options.outputPath);

      // Take the html snapshots of the pages
      htmlSnapshots.run({
          input: 'array',
          source: pageList,
          processLimit: options.concurrency,
          outputDir: outputDir,
          outputDirClean: options.outputDirClean,
          selector: {
            '__default': 'div[id=finished]'
          },
          useJQuery: {
            '__default': false
          },
          checkInterval: 250,
          pollInterval: 500,
          timeout: options.timeout,
          snapshotScript: {
            script: 'removeScripts'
          }
        },
        // Callback. Called when everything is done
        function(err, snapshotsCompleted) {
          gutil.log(gutil.colors.blue(gutil.colors.bold(snapshotsCompleted.length) + ' snapshots completed.'));
          if (err) {
            gutil.log(gutil.colors.red('Failed to shapshot ' + gutil.colors.bold(pageList.length - snapshotsCompleted.length) + ' pages'));
          }

          // Return the same input file for further process
          self.push(file);

          cb(); // Done.
        }
      );
    }

    // If file is stream, emit an error (we don't support streams for this plugin)
    if (file.isStream()) {
      self.emit('error', new PluginError('gulp-snapshots', 'Streams are not supported!'));
      cb(); // Done.
    }

  });
}

gulp.task('waitserver', function (done) {
  gutil.log(gutil.colors.blue('Waiting 5 seconds for server to start...'))
  setTimeout(function () {
    gutil.log(gutil.colors.blue('Finished to wait'))
    done();
  }, 5000);
})

gulp.task('snapshots', function() {
  return gulp.src('pageList.json')
    .pipe(gulpSnapshots({
      siteUrl: 'http://localhost:3000', // This is localhost because snapshots are created serving local files
      concurrency: 16,
      outputDirClean: true,
      timeout: 40000,
      outputPath: path.join(conf.paths.dist + '/snapshots')
    }));
});

gulp.task('sitemap', function () {
  return gulp.src(path.join(conf.paths.dist + '/snapshots/**/*.html'))
    .pipe(sitemap({
      siteUrl: 'http://your-website-url-here.com' // TODO: change this to match your URL
    }))
    .pipe(gulp.dest(conf.paths.dist));
});

gulp.task('exit', function (done) {
  gutil.log('Exiting');
  done();
  process.exit();
});

gulp.task('build:with-seo', function (callback) {
  runSequence('serve:seo', 'waitserver', 'snapshots', 'sitemap', 'exit', callback);
})